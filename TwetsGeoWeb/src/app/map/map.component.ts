import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as L from 'leaflet';
import 'leaflet.markercluster';
import 'leaflet/dist/images/marker-icon-2x.png'
import 'leaflet/dist/images/marker-shadow.png'
import 'leaflet-heatmap'
import 'heatmap.js'
import { buffer, point } from "@turf/turf"
import { TweetsService } from "../services/tweets.service";

declare var HeatmapOverlay;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  private map;
  private turf;
  private markers;
  private data;
  private heatmapLayer;
  cfg = {
    // radius should be small ONLY if scaleRadius is true (or small radius is intended)
    // if scaleRadius is false it will be the constant radius used in pixels
    "radius": 15,
    "maxOpacity": .5, 
    // scales the radius based on map zoom
    "scaleRadius": false, 
    // if set to false the heatmap uses the global maximum for colorization
    // if activated: uses the data maximum within the current map boundaries 
    //   (there will always be a red spot with useLocalExtremas true)
    "useLocalExtrema": true,
    // which field name in your data represents the latitude - default "lat"
    latField: 'lat',
    // which field name in your data represents the longitude - default "lng"
    lngField: 'lng',
    // which field name in your data represents the data value - default "value"
    valueField: 'count',
    blur: 0.95,
    gradient: { 
    '.5': 'yellow',
    '.8': 'orange',
    '.95': 'red'
    }
  };

  constructor(private tweets: TweetsService) { }

  ngOnInit() {
    this.tweets.allTweets()
    .subscribe(
      res => {
        console.log(res)
        this.data = res['data'];
        this.clusterPoints();
        this.initMap();
      },
      err =>{
        console.log(err)
      }
    );
    //this.clusterPoints();
    //this.headMap();
  }

  private initMap(): void {
    this.map = L.map('map')
    .setView([19.419444, -99.145556], 13);
    const basemap =L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox/dark-v10',
      tileSize: 512,
      zoomOffset: -1,
      accessToken: 'pk.eyJ1IjoidGViYW4xOTk3IiwiYSI6ImNrZzg4NzhiMTBlbHUycW8yamVheTlhcnQifQ.OGEyzvbgUhb-Yhr7bJfoMQ'
    })
    basemap.addTo(this.map);
    this.markers.addTo(this.map);
    var min = 1;
    var max = 3910;
    var testData = {
      max: max,
      data: this.data
    };
    this.heatmapLayer = new HeatmapOverlay(this.cfg).addTo(this.map);
    this.heatmapLayer.setData(testData);

    var baseMaps = {
      "Mapa Base": basemap,
    };
    var overlayMaps = {
      "Cluster": this.markers,
      "HeatMap": this.heatmapLayer
    };

    L.control.layers(baseMaps, overlayMaps,{
      position: 'topright', // 'topleft', 'bottomleft', 'bottomright'
      collapsed: false, // true
      autoZindex: true
    }).addTo(this.map);


  }


  clusterPoints(){
    this.markers = L.markerClusterGroup({ chunkedLoading: false });
    for (var i = 0; i <  50000; i++) {
      var a = this.data[i];
      var title = a.Lat;
      var marker = L.marker(new L.LatLng(a.lat, a.lng), { title: title });
      marker.bindPopup(title);
      this.markers.addLayer(marker);
    }
  }


  headMap(){
    
    
  }
}
