// config.js
const dotenv = require('dotenv');
const path = require('path');


dotenv.config({
  path: path.resolve(__dirname, process.env.NODE_ENV + '.env')
});

module.exports = {
  NODE_ENV: process.env.NODE_ENV,
  twitter: {
    TWITTER_API_KEY: process.env.TWITTER_API_KEY,
    TWITTER_SECRET_KEY: process.env.TWITTER_SECRET_KEY,
    TWITTER_BARER_TOKEN: process.env.TWITTER_BARER_TOKEN,
  },
  dir: {
    DATA_DIR: process.env.DATA_DIR
  },
}