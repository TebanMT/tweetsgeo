# Web Mapping para la Representación de Volumen de Tweets Alusivos al covid19 Mediante Mapa de Calor y Clustering

Se analizan los datos georreferenciados y que hagan alusión a casos positivos de covid19 al rededor del mundo por medio de la red social Twitter, con el fin de conocer los países con mayor contagio en relación directa con el número de casos positivos. La representación de la información se hace atrevés del proveedor de mapas Mapbox con las herramientas de la librería leaflet y la librería heatmap.js para la creación de un mapa de calor. Para la integración de las fuentes de datos, el procesamiento y la representación se utiliza la plataforma javascript.

## Requisitos

- Node.js v12
- Angular 10

###### Si se requiere hidratar los Tweets los requisitos son

- Python 3
- Cuenta de desarrollador en Twitter
- Credenciales de Twitter

## ¿ Cómo ejecutar el proyecto en local ?

###### Ejecutar el servidor Node.js
Dentro de la carpeta twets-geo_core
```
1.- npm install o npm i
2.- npm run dev
```
En el archivo development.env que se encuentra en la carpeta config se debe configurar la ruta absoluta al archivo coordenadas.csv que se encuentra en la carpeta COORDENADASDATASET.
La clave donde se debe colocar dicha ruta es DATA_DIR

###### Ejecutar la aplicación Web
Dentro de la carpeta TwetsGeoWeb

```
1.- npm install o npm i
2.- ng serve
```
Para que funcione de manera correcta, verificar que el servidor node.js se ejecute en el puerto 3000

###### Si se quiere hidratar los Tweets
Dentro de la carpeta scripts

```
1.- pip install -r requirements.txt
2.- python coordenadas.py
```

Se puden modificar las rutas para hidratar distintos datasets, pero para replicar el proyecto no es necesario volver a hidratar los tweets.

##### Esta es una prueba de concepto por lo que aun quedan muchas cosas que mejorar en la arquitectura y configuración del proyecto.

## Estado
#### Finalizado con articulo (no publicado)
