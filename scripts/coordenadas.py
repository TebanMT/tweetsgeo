"""
    Autor    : Esteban Mendiola Téllez
    Contacto : Mendiola_esteban@outlook.com

    Desc: Se crea una funcion para agregar los Headers 'Lat' y 'Long'
          a un conjunto de archivos csv.

"""


import os
import pandas as pd
from twarc import Twarc

ROOT_DATA_DIR =  '../GEODATASET/'
ROOT_DATA_DEST = '../COORDENADASDATASET/'

consumer_key=""
consumer_secret=""
access_token=""
access_token_secret=""

def add_coordenadas():
    data = {}
    lat = []
    lng = []
    datasets = [item for item in os.listdir(ROOT_DATA_DIR)
                if os.path.isfile(os.path.join(ROOT_DATA_DIR, item))]
    t = Twarc(consumer_key, consumer_secret, access_token, access_token_secret)
    for d in datasets:
        print(d)
        try:
            for tweet in t.hydrate(open(ROOT_DATA_DIR+d)):
                try:
                    if tweet["coordinates"] == None:
                        continue
                    longitude, latitude = tweet["coordinates"]["coordinates"]
                    lat.append(latitude)
                    lng.append(longitude)
                except Exception as ex:
                    print("error: ,",ex)
                    continue
        except Exception as ex:
            print("error: ",ex)
            pass
    data['Lat']  = lat
    data['Long'] = lng
    df = pd.DataFrame(data, columns= ['lat', 'lng'])
    df.to_csv(ROOT_DATA_DEST+'coordenadas.csv', index=False)


if __name__ == "__main__":
    add_coordenadas()