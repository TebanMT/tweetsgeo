/**
 * @author Esteban Mendiola <mendiola_esteban@outlook.com>
 */

const csv = require('csv-parser');
const fs = require('fs');
const config = require('../../configs/config').dir;

const results = [];
async function loadCSV(){
    return datos = new Promise((resolve, reject) =>{
        fs.createReadStream(config.DATA_DIR)
        .pipe(csv())
        .on('data', (row)=>{
            row.count = 1;
            results.push(row);
        })
        .on('end', ()=>{
            resolve(results);
        });
    });
}


module.exports = {
    loadCSV
}