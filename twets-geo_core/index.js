/**
 * Main application file
 * @author Esteban Mendiola <mendiola_esteban@outlook.com>
 */

const express = require('express');
var cors = require('cors');
const winston = require('winston');
const morgan = require('morgan');
const twets = require('./src/routes/twets');

const app = express();
//CORS (Cross-Origin Resource Sharing) headers to support Cross-site HTTP requests
app.use(cors());

app.set('port', process.env.PORT || 3000);
app.set('json spaces', 4);
app.use(morgan('dev'));
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb', extended: true }));

//Routes
app.use('/api/twets', twets);
app.route('/')
.get((req, res) => {
    res.end("TWETS GEO API");
});

//Staring Server
app.listen(app.get('port'), () =>{
    winston.info(`Server on port ${app.get('port')} in ${app.get('env')} mode`);
});