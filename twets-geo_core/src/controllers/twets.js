/**
 * @author Esteban Mendiola <mendiola_esteban@outlook.com>
 */

const twitter = require('../../configs/config').twitter;
const Twitter = require('twitter');
const util = require('../util/tweets')

var client = new Twitter({
    consumer_key: twitter.TWITTER_API_KEY,
    consumer_secret: twitter.TWITTER_SECRET_KEY,
    access_token_key : "your token key",
    access_token_secret: "your token secret"
});

async function twets(req, res){
    client.stream('statuses/filter', {track: '#Covidiotas'},  function(stream) {
        stream.on('data', function(tweet) {
            if (tweet.geo != null ) {
                console.log(tweet.text)
                console.log(tweet.geo);
                console.log(tweet.coordinates);
            }
        });
      
        stream.on('error', function(error) {
          console.log(error);
        });
      });
}

async function allTweets(req, res) {
    util.loadCSV()
    .then(data =>{
        res.status(200).json({data: data});
    })
    .catch(err =>{
        res.status(500).json({err: err});
    });
}

module.exports = {
    allTweets
}
